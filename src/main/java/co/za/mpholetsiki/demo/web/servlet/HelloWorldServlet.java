package co.za.mpholetsiki.demo.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 */
public class HelloWorldServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setAttribute("customerFunction", "test function");
    // RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/helloworld.jsp");
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("Helloworld directly via outputstream");
    out.println("</html>");
    // dispatcher.forward(request, response);
  }

  @Override
  protected void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    System.out.println("\n***** NEW REQUEST *****");
    System.out.println("In HelloWorldServlet : Service Method");
    System.out.println("Request Class: " + request.getClass() + "\n");
    System.out.println("Response Class: " + response.getClass() + "\n");
    for (Enumeration<String> e = request.getHeaderNames(); e.hasMoreElements();) {
      String headerName = e.nextElement();
      System.out.println("Header Name: " + headerName + " Value: " + request.getHeader(headerName));
    }

    System.out.println("\nProtocol: " + request.getProtocol());
    System.out.println("Method: " + request.getMethod());
    System.out.println("Request URI: " + request.getRequestURI());
    System.out.println("PathInfo: " + request.getPathInfo());

    super.service(request, response);
  }

}
