drop table Customer;
create table Customer (
  id int4 not null, 
  firstName varchar(255) not null, 
  surname varchar(255) not null, 
  dateOfBirth timestamp not null,
  primary key (id)
);
